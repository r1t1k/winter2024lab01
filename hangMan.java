import java.util.Scanner;						//  I listened to your advise and made it so the word can be of any length.  //
public class Hangman{
	public static void main(String[] args){ 	
	// Asks the user for a word and clears the screen for their friend to play hangman with that word																		
		System.out.println();		
		Scanner reader = new Scanner(System.in);
		//get user input
        System.out.print("Give the WORD you want your Friend to Guess: ");
        String userWordTarget= reader.nextLine();
		userWordTarget = userWordTarget.toUpperCase();
		//Convert to upper case
		System.out.print("\033c");
		runGame(userWordTarget);
	}
	
	public static void runGame(String word){
	//Takes in a word then prompts the user to guess the word letter by letter with only 6 wrong guesses.
		int guessesLeft = 6;		
		// used to tell the user the number of guesses left.
		boolean[] letters = new boolean[word.length()];
		boolean correct = false;// to check if the user has guessed the word 
		String lettersGuessedWrong = "";
		// used to tell the user which letters they have guessed wrong.
		while(guessesLeft > 0 && !correct) {																	// Conditions when the game is being played
		//This loop checks if the user has any guesses or has not gotten the word right and stops the game if either is false.
			Scanner UserGuessing = new Scanner(System.in);
			//to get user's letter guess
			System.out.print("Guess a letter: ");
			char userGuess= UserGuessing.next().charAt(0);
			userGuess = toUpperCase(userGuess);
			// made the user's guess upper cased here instead of in every argument i needed the upper cased user guess
			if(isLetterInWord(word, userGuess) == -1){														// if wrong lets the user know
				guessesLeft = guessesLeft - 1;
				System.out.println("Haaaa!!\nYour Guess was WRONG!\n");
			}
			else{																										//Checks whether letter is in the word and lets you know if it is and if it has already been guessed.
				if(!(letters[isLetterInWord(word, userGuess)])){
					letters[isLetterInWord(word, userGuess)] = true;
					System.out.println("Your Guess was CORRECT!!!\nBeginners Luck!\nRevealing Letter:\n");
				}
				else{
					System.out.println("You Have Already Guessed This Letter.\nI think you are blind and might have alzheimer's\nGuess Again!");
				}
			}
			printWork(word, letters);																					// Displays the word with the letters the user has guessed.
			
			if(guessesLeft > 1){
				System.out.println("You have " + guessesLeft + " Wrong Guesses Left.\n");							 	//Lets the user know how many guesses are left
			}
			else{
				System.out.println("You have " + guessesLeft + " Wrong Guess Left.\n");
			}
			
			if(isLetterInWord(word, userGuess) == -1){														//Displays letters guessed wrong
				lettersGuessedWrong += userGuess + "" + ", ";
			}
			if(lettersGuessedWrong.length() > 0){
				System.out.println("These are the Letters you have Guessed Wrong: \n" + lettersGuessedWrong + " \n\n");
			}
			
			correct = true;
			for(int i = 0; i < letters.length; i++){																
				if(!letters[i]){
					correct = false;
				}
			}
			//sets the word to guessed correctly then checks if any letters are not guessed and changes the correct to false
			//so the loop continues until they have either guessed the word correctly or have run out of guesses
			
			if(correct){																						//Final comments to end the game.(Based on your result)
				System.out.println("Good Job!\nThe word was " +  word + "\nYou have some potential but you could still be better.");
			}
			if(guessesLeft == 0){
				System.out.println("Haaaa!!! \nYou Suck at guessing words!\nThe word was " + word + ".\nYou should go cry in your room.\nSkill Issue For real.\nDo Better.");
			}
		}
	}
	public static void printWork(String word, boolean[] letters){
	// Check if the letter is guessed and displays the letter instead of the Underscore.
		String display = "";
		for(int i = 0; i < letters.length; i++){
			if(letters[i] == true)
				display += word.charAt(i);
			else
				display += "_";
		}
		System.out.println(display);
	}
	public static int isLetterInWord(String word, char c){
	//Check if the letter is in the word and return where in the word it is. If not in the word returns -1.
		for(int i = 0; i < word.length(); i++){
			if(word.charAt(i) == c)
				return i;
		}
		return -1;
	}
	public static char toUpperCase(char c){
	// Makes the character(s) upper cased.
		return Character.toUpperCase(c);
	}
}